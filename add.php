<?php $loadEditor = true; require_once('_header.php');?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Snippet</h1>
                    <p>
						<div class="form-group">
						    <label>Name</label>
						    <input class="form-control" id="name">
						</div>
						<div class="form-group">
						    <label>Description</label>
						    <textarea class="form-control" rows="3" name="desription"></textarea>
						</div>
                    </p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row file" id="file-1">
            	<div class="col-lg-12">
	            	<div class="form-group">
	            		<label>Filename</label>
	            		<input class="form-control file-name">
	            	</div>
	            	<div class="form-group">
	            		<label>Language</label>
		            	<select class="form-control file-language">
							<!-- <option value="">-- Select a Language --</option> -->

							<option value="actionscript">ActionScript</option>
							<option value="batchfile">Batch File</option>
							<option value="c_cpp">C/C++</option>
							<option value="clojure">Clojure</option>
							<option value="coffee">Coffee Script</option>
							<option value="csharp">C#</option>
							<option value="css">CSS</option>
							<option value="dockerfile">Docker File</option>
							<option value="elixir">Elixir</option>
							<option value="gitignore">.gitignore</option>
							<option value="html">HTML</option>
							<option value="java">Java</option>
							<option value="javascript" selected>JavaScript</option>
							<option value="json">JSON</option>
							<option value="jsp">JSP</option>
							<option value="less">LESS</option>
							<option value="makefile">make File</option>
							<option value="markdown">Markdown</option>
							<option value="mysql">MySQL</option>
							<option value="perl">PERL</option>
							<option value="pgsql">PG SQL</option>
							<option value="php">PHP</option>
							<option value="plain_text">Plain Text</option>
							<option value="python">Python</option>
							<option value="ruby">Ruby</option>
							<option value="sass">SASS</option>
							<option value="sh">.sh</option>
							<option value="sql">SQL</option>
							<option value="sqlserver">SQL Server</option>
							<option value="svg">SVG</option>
							<option value="swift">Swift</option>
							<option value="typescript">TypeScript</option>
							<option value="vbscript">VBScript</option>
							<option value="xml">XML</option>
							<option value="yaml">YAML</option>
		            	</select>
		            </div>
		            <div id="editor" class="editor"></div>
		        </div>
            </div>



<?php require_once('_footer.php');?>

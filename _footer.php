        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>
    <script src="js/metisMenu/metisMenu.min.js"></script>

    <script src="js/site.min.js"></script>
    <?php if($loadEditor): ?>
    	<script src="js/ace/ace.js" type="text/javascript" charset="utf-8"></script>
    	<script>
    	    var editor = ace.edit("editor");
    	    editor.setTheme("ace/theme/monokai");
    	    editor.getSession().setMode("ace/mode/javascript");
    	</script>
    <?php endif; ?>
  </body>
</html>